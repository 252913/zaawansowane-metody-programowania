#ifndef LIBINTERFACE_HH
#define LIBINTERFACE_HH

#include "Interp4Command.hh"

#include <string>
#include <iostream>
#include <map>
#include <dlfcn.h>




class LibraryInterface{
private:

    void *libHandler;

public:

    LibraryInterface(std::string Lib_Name); 
    ~LibraryInterface();
    Interp4Command *(*pCreateCmd)(void);
    std::string name;
};   
#endif