
#ifndef SET4LIBINTERFACES_HH
#define SET4LIBINTERFACES_HH

#include "LibInterface.hh"
#include <memory>

typedef std::map<std::string, std::shared_ptr<LibraryInterface>> LibMap;

class Set4LibraryInterfaces{
private:
  LibMap _libMap;

public:
  Set4LibraryInterfaces();
  ~Set4LibraryInterfaces();
  void LoadLibrary(std::string path);
  LibMap::iterator FindLibrary(std::string libName);
  LibMap::iterator EndMap();
};

#endif