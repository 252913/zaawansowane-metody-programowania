#include <iostream>
#include "Interp4Set.hh"
#include "MobileObj.hh"

using std::cout;
using std::endl;


extern "C" {
 Interp4Command* CreateCmd(void);
  const char* GetCmdName() { return "Set"; }
}




/*!
 * \brief
 *
 *
 */
Interp4Command* CreateCmd(void)
{
  return Interp4Set::CreateCmd();
}




/*!
 *
 */
Interp4Set::Interp4Set(): _position_x(0), _position_y(0), _position_z(0),
                            _angle_x(0), _angle_y(0), _angle_z(0)
{}


/*!
 *
 */
void Interp4Set::PrintCmd() const
{
  /*
   *  Tu trzeba napisać odpowiednio zmodyfikować kod poniżej.
   */
  cout << GetCmdName() << "\t" << _position_x<< "\t"  << _position_y<< "\t"  << _position_z << endl;
  cout << GetCmdName() << "\t" << _angle_x<< "\t"  << _angle_y<< "\t"  << _angle_z  << endl;
}


/*!
 *
 */
const char* Interp4Set::GetCmdName() const
{
  return ::GetCmdName();
}


/*!
 *
 */
bool Interp4Set::ExecCmd( MobileObj  *pMobObj,  int  Socket) const
{
  /*
   *  Tu trzeba napisać odpowiedni kod.
   */
  return true;
}


/*!
 *
 */
bool Interp4Set::ReadParams(std::istream& Strm_CmdsList)
{
  /*
   *  Tu trzeba napisać odpowiedni kod.
   */
  Strm_CmdsList >> _name >> _position_x >> _position_y >> _position_z >> _angle_x >> _angle_y >> _angle_z;
  return true;
}


/*!
 *
 */
Interp4Command* Interp4Set::CreateCmd()
{
  return new Interp4Set();
}


/*!
 *
 */
void Interp4Set::PrintSyntax() const
{
  cout << "   Set  NazwaObiektu  Kat_x[rad] Kat_y [rad]  Kat_z[rad]" << endl;
  cout << "   Set  NazwaObiektu  Pozycja_x[mm] Pozycja_y[mm]  Pozycja_z[mm]" << endl;
}
