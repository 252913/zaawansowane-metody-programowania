#include "LibInterface.hh"

//Constructor
LibraryInterface::LibraryInterface(std::string Lib_Name){

    libHandler = dlopen(Lib_Name.c_str(), RTLD_LAZY);


    if (!libHandler)
    {
        std::cerr << "Library " << Lib_Name <<" hasn't been found."<< std::endl;
    }
    else
    {
        std::cout << "Library " << Lib_Name <<" has been found."<< std::endl;
    }

  
    void *cmd = dlsym(libHandler, "CreateCmd");


    if (!cmd)
    {
        std::cerr << "Cannot find this function: " << Lib_Name  << std::endl;
    }

   
    pCreateCmd = *reinterpret_cast<Interp4Command *(*)(void)>(cmd);             //Creating new command.


    Interp4Command *Inter_Command = pCreateCmd();


    name = Inter_Command->GetCmdName();


    delete Inter_Command;
}




//Destructor
LibraryInterface::~LibraryInterface()
{
    dlclose(libHandler);
}