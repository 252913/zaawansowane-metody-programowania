#include "Set4LibInterfaces.hh"

// Konstruktor
Set4LibraryInterfaces::Set4LibraryInterfaces(){
    LoadLibrary("libs/libInterp4Move.so");
    LoadLibrary("libs/libInterp4Set.so");
    LoadLibrary("libs/libInterp4Rotate.so");
    LoadLibrary("libs/libInterp4Pause.so");
}

Set4LibraryInterfaces::~Set4LibraryInterfaces(){

}

void Set4LibraryInterfaces::LoadLibrary(std::string path){

    std::shared_ptr<LibraryInterface> pLibrary = std::make_shared<LibraryInterface>(path);
    _libMap.insert({pLibrary->name, pLibrary});
}

LibMap::iterator Set4LibraryInterfaces::FindLibrary(std::string libName){
    return _libMap.find(libName);
}

LibMap::iterator Set4LibraryInterfaces::EndMap(){
    return _libMap.end();
}