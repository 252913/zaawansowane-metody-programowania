#include <iostream>
#include <dlfcn.h>
#include <cassert>
#include <sstream>
#include <map>
#include <cstdio>

#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/util/XMLString.hpp>
#include "xmlinterp.hh"
#include <list>


#include "Interp4Command.hh"
#include "Set4LibInterfaces.hh"
#include "MobileObj.hh"



#define LINE_SIZE 500

using namespace std;
using namespace xercesc;

bool ExecPreprocesor(const char *FileName, istringstream &IStrm4Cmds){
  string Cmd4Preproc = "cpp -P ";
  char Line[LINE_SIZE];
  ostringstream OTmpStrm;

  Cmd4Preproc += FileName;
  FILE *pProc = popen(Cmd4Preproc.c_str(), "r");

  if (!pProc)
    return false;

  while (fgets(Line, LINE_SIZE, pProc)){
    OTmpStrm << Line;
  }

  IStrm4Cmds.str(OTmpStrm.str());

  return pclose(pProc) == 0;
}




bool ExecActions(istringstream &iStrm, Set4LibraryInterfaces &LibraryList){
  string command;
  string object;

  while (iStrm >> command)                                                    //WYKONUJE DOPÓKI JEST JAKIŚ TEKST W STRUMIENIU
  { 

    
    LibMap::iterator cmd_iterator = LibraryList.FindLibrary(command);         //CZY KOMENDA ISTNIEJE

    if (cmd_iterator == LibraryList.EndMap())
    {                                                                         //POPRAWNOŚĆ KOMENDY - PARAMETRY NAZWA ITP.
      return false;
    }

  
    Interp4Command *pCommand = cmd_iterator->second->pCreateCmd();            //TWORZENIE NOWEJ KOMENDY



    if (!pCommand->ReadParams(iStrm))
    {  
      return false;
    }

    cout << "One move"<<endl;

    
    pCommand->PrintCmd();
  }
  return true;
}




/*!
 * Czyta z pliku opis poleceń i dodaje je do listy komend,
 * które robot musi wykonać.
 * \param sFileName - (\b we.) nazwa pliku z opisem poleceń.
 * \param CmdList - (\b we.) zarządca listy poleceń dla robota.
 * \retval true - jeśli wczytanie zostało zrealizowane poprawnie,
 * \retval false - w przeciwnym przypadku.
 */
bool ReadFile(const char* sFileName, Configuration &rConfig)
{
   try {
            XMLPlatformUtils::Initialize();
   }
   catch (const XMLException& toCatch) {
            char* message = XMLString::transcode(toCatch.getMessage());
            cerr << "Error during initialization! :\n";
            cerr << "Exception message is: \n"
                 << message << "\n";
            XMLString::release(&message);
            return 1;
   }

   SAX2XMLReader* pParser = XMLReaderFactory::createXMLReader();

   pParser->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);
   pParser->setFeature(XMLUni::fgSAX2CoreValidation, true);
   pParser->setFeature(XMLUni::fgXercesDynamic, false);
   pParser->setFeature(XMLUni::fgXercesSchema, true);
   pParser->setFeature(XMLUni::fgXercesSchemaFullChecking, true);

   pParser->setFeature(XMLUni::fgXercesValidationErrorAsFatal, true);

   DefaultHandler* pHandler = new XMLInterp4Config(rConfig);
   pParser->setContentHandler(pHandler);
   pParser->setErrorHandler(pHandler);

   try {
     
     if (!pParser->loadGrammar("config/config.xsd",
                              xercesc::Grammar::SchemaGrammarType,true)) {
       cerr << "!!! Plik grammar/actions.xsd, '" << endl
            << "!!! ktory zawiera opis gramatyki, nie moze zostac wczytany."
            << endl;
       return false;
     }
     pParser->setFeature(XMLUni::fgXercesUseCachedGrammarInParse,true);
     pParser->parse(sFileName);
   }
   catch (const XMLException& Exception) {
            char* sMessage = XMLString::transcode(Exception.getMessage());
            cerr << "Informacja o wyjatku: \n"
                 << "   " << sMessage << "\n";
            XMLString::release(&sMessage);
            return false;
   }
   catch (const SAXParseException& Exception) {
            char* sMessage = XMLString::transcode(Exception.getMessage());
            char* sSystemId = xercesc::XMLString::transcode(Exception.getSystemId());

            cerr << "Blad! " << endl
                 << "    Plik:  " << sSystemId << endl
                 << "   Linia: " << Exception.getLineNumber() << endl
                 << " Kolumna: " << Exception.getColumnNumber() << endl
                 << " Informacja: " << sMessage 
                 << endl;

            XMLString::release(&sMessage);
            XMLString::release(&sSystemId);
            return false;
   }
   catch (...) {
            cout << "Zgloszony zostal nieoczekiwany wyjatek!\n" ;
            return false;
   }

   delete pParser;
   delete pHandler;
   return true;
}



int main(int argc, char *argv[])
{
  Set4LibraryInterfaces LibraryList;                //DOSTĘPNE BIBLIOTEKI 
  istringstream iStrm;                              //KOMENDY
   Configuration   Config;

   if (!ReadFile("config/config.xml",Config)) return 1;
  
  ExecPreprocesor("COMMANDS.txt", iStrm);
  cout << endl << "Ruchy drona: "<<endl;
  cout<< iStrm.str() << endl;


  if (!ExecActions(iStrm, LibraryList))             //UZYSKIWANIE POSZCZEGÓLNYCH KOMEND
  {
    return 0;
  }

}
